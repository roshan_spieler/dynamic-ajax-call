"use strict";
/*
 * Synchronous and Asynchronous dynamic ajax call V1.0
 *
 * Copyright (c) 2017 Roshan Patil
 * Licensed under the MIT License:
 *   http://www.opensource.org/licenses/mit-license.php
 */

// type: Method type, URL: URL to get data, data: data to be sent


    function syncAjax(type,url,data)
    {
           return $.ajax({
                        url: url ,
                        type:type,
                        data:data,
						async: false,
                        success: function(data) {
                           return data;
                        }
                    }).responseText;
        
    }
	
	function AsyncAjax(type,url,data,result)
    {
           return $.ajax({
                        url: url ,
                        type:type,
                        data:data,
						success: function(data) {
							result(data);
                        }
                    }).responseText;
        
    }
	
/*
	
	Example : 
	
		console.log('fun1==>', 
							AsyncAjax('post',
									   apiurl ,
							           $('form').serialize(),
										function(data){
											console.log('fun1==>', data);
										})
							);
				console.log('fun2==>',
										syncAjax('post',
												  apiurl,
												  $('form').serialize()
											    )
							);			
							
*/							
